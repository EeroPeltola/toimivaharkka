package com.example.n6723.vk11uusyritys;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

public class YourReservations extends AppCompatActivity {

    Context context = null;
    private ArrayAdapter mAdapter, mAdapter2, mAdapter3;
    GymManager gymManager = GymManager.getInstance();
    XMLWriter xmlWriter = new XMLWriter();
    CSVWriter csvWriter = new CSVWriter();
    InputStream inStr;
    Spinner spinner, spinnerForOwnReservations;
    ArrayList<String> gymData;
    ListView listView;
    String username;
    MainActivity mainActivity = new MainActivity();

    // ########## USE, PURPOSE, TIME #######
    EditText kaytto;
    public String purpose = "";
    private String timeStart = "00:00";
    private String timeEnd = "23:59";
    // ################################################

    // ############# DATE ###################
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener; // HOX
    private static final String TAG = "MainActivity";
    public String date;
    // #######################################

    // ######## START TIME #################
    private TextView timelist1;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener; // HOX
    private static final String TAG2 = "MainActivity";
    private int testTime1 = 0000;
    //###################################################

    // ######## END TIME #################
    private TextView timelist2;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener2; // HOX
    private static final String TAG3 = "MainActivity";
    private int testTime2 = 2359;
    //###################################################


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_reservations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //###### Set this date as a default ##########################
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month++;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        date = day + ":" + month + ":" + year;
        String[] date1parts = date.split(":");
        String day1 = date1parts[0];
        String month1 = date1parts[1];
        String year1 = date1parts[2];

        if (day1.length() == 1) {
            day1 = "0" + day1;
        }
        if (month1.length() == 1) {
            month1 = "0" + month1;
        }
        date = day1 + "." + month1 + "." + year1;
        //######################################################

        username = mainActivity.getUsername();

        spinner = findViewById(R.id.spinner);
        spinnerForOwnReservations = findViewById(R.id.spinnerForOwnReservations);
        kaytto = (EditText) findViewById(R.id.tarkoitus);

        // ############ date selection ############################
        // Open to user calendar view and saves the date to a variable 'date'

        mDisplayDate = (TextView) findViewById(R.id.tvDate);
        listView = (ListView) findViewById(R.id.listView);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        YourReservations.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = day + ":" + month + ":" + year;

                String[] date2parts = date.split(":");
                String day2 = date2parts[0];
                String month2 = date2parts[1];
                String year2 = date2parts[2];

                if (day2.length() == 1) {
                    day2 = "0" + day2;

                }
                if (month2.length() == 1) {
                    month2 = "0" + month2;
                }

                date = day2 + "." + month2 + "." + year2;
                mDisplayDate.setText(date);

            }
        };
        //##############################################################


        // ############### Start time selection ############################
        // Open to user clock view and saves the time to a variable 'timeStart'
        timelist1 = (TextView) findViewById(R.id.time1);

        timelist1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal2 = Calendar.getInstance();
                int hour = cal2.get(Calendar.HOUR);
                int minute = cal2.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(
                        YourReservations.this,
                        android.R.style.Widget_Holo_Light,
                        mTimeSetListener,
                        hour, minute, true);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });
        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Log.d(TAG2, "onTimeSet: hh/mm: " + hourOfDay + "/" + minute);

                timeStart = hourOfDay + ":" + minute;

                timeStart = hourOfDay + ":" + minute;
                String[] sparts = timeStart.split(":");
                String stestTime = sparts[0];
                String slastPart = sparts[1];

                if (stestTime.length() == 1) {
                    stestTime = "0" + stestTime;
                }
                if (slastPart.length() == 1) {
                    slastPart = "0" + slastPart;
                }

                timeStart = stestTime + ":" + slastPart;
                testTime1 = Integer.parseInt(stestTime) + Integer.parseInt(slastPart);

                if (testTime1 >= testTime2) {
                    timeStart = "00:00";
                    Toast.makeText(getBaseContext(), "Ending time must be bigger than starting time!", Toast.LENGTH_LONG).show();
                }
                timelist1.setText(timeStart);
            }
        };
        // ###########################################################


        // ############### end time selection ############################
        // Open to user clock view and saves the time to a variable 'timeEnd'
        timelist2 = (TextView) findViewById(R.id.time2);


        timelist2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal3 = Calendar.getInstance();
                int hour2 = cal3.get(Calendar.HOUR);
                int minute2 = cal3.get(Calendar.MINUTE);

                TimePickerDialog timeDialog2 = new TimePickerDialog(
                        YourReservations.this,
                        android.R.style.Widget_Holo_DropDownItem_Spinner,
                        mTimeSetListener2,
                        hour2, minute2, true);
                timeDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog2.show();
            }
        });
        mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay2, int minute2) {
                Log.d(TAG3, "onTimeSet: hh:mm: " + hourOfDay2 + ":" + minute2);

                timeEnd = hourOfDay2 + ":" + minute2;
                String[] eparts = timeEnd.split(":");
                String etestTime = eparts[0];
                String elastPart = eparts[1];

                if (etestTime.length() == 1) {
                    etestTime = "0" + etestTime;

                }
                if (elastPart.length() == 1) {
                    elastPart = "0" + elastPart;
                }

                timeEnd = etestTime + ":" + elastPart;
                testTime2 = Integer.parseInt(etestTime) + Integer.parseInt(elastPart);

                if (testTime1 >= testTime2) {
                    timeEnd = "23:59";
                    Toast.makeText(getBaseContext(), "Ending time must be bigger than starting time!", Toast.LENGTH_LONG).show();
                }
                timelist2.setText(timeEnd);
            }
        };
        // ###############################################

        ArrayList<String> gymNameArrayList = new ArrayList<String>();
        gymData = new ArrayList<String>();
        listView = findViewById(R.id.listView);

        context = YourReservations.this;

        try {
            inStr = context.openFileInput("gymXML.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        gymManager.readXML(inStr);

        gymNameArrayList.add("Small Gym");
        gymNameArrayList.add("Big Gym");
        gymNameArrayList.add("Workout Gym");
        gymNameArrayList.add("Tennis Gym");

        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymNameArrayList);
        spinner.setAdapter(mAdapter);

        View v = null;
        pushSearch(v);
    }

    // Search own reservations
    public void pushSearch(View v) {

        purpose = kaytto.getText().toString();
        gymData = gymManager.getGymReservationDataByUsername2(username);
        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymData);
        spinnerForOwnReservations.setAdapter(mAdapter2);
    }

    // Same as in NewReservation
    public void pushAdd(View v) {

        purpose = kaytto.getText().toString();

        int selection = spinner.getSelectedItemPosition();
        String gymname = "";

        if (selection == 0) {
            gymname = "Small Gym";
        } else if (selection == 1) {
            gymname = "Big Gym";
        } else if (selection == 2) {
            gymname = "Workout Gym";
        } else if (selection == 3) {
            gymname = "Tennis Gym";
        }

        if (purpose.equals("") == true) {
            Toast.makeText(getBaseContext(), "Please add purpose!", Toast.LENGTH_LONG).show();
        } else if ((gymManager.addReservationAdder(gymname, timeStart, timeEnd, date, username, purpose)) == false) {
            Toast.makeText(getBaseContext(), ("Reservation for " + date + " couldn't be made!"), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), "Reservation made!", Toast.LENGTH_LONG).show();
            csvWriter.wCSV(context);
            xmlWriter.writeXML(context);
        }
    }

    // Cancel user's own selected reservation
    public void pushCancelReservation (View v) {

        if (spinnerForOwnReservations.getSelectedItem() == null) {
            Toast.makeText(getBaseContext(), "You haven't chosen any reservation", Toast.LENGTH_LONG).show();
        } else {

            int selection2 = spinnerForOwnReservations.getSelectedItemPosition();

            gymManager.deleteReservation2(selection2, username);
            csvWriter.wCSV(context);
            xmlWriter.writeXML(context);
            pushSearch(v);
        }
    }

    // Allows user to change own reservation
    public void changeReservation (View v) {

        int selection2 = spinnerForOwnReservations.getSelectedItemPosition();


        gymManager.deleteReservation2(selection2, username);
        pushAdd(v);
        csvWriter.wCSV(context);
        xmlWriter.writeXML(context);
        pushSearch(v);
    }

}