package com.example.n6723.vk11uusyritys;

import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GymManager {

    private ArrayList<Gym> gymArrayList;
    Gym smallGym, bigGym, workoutGym, tennisGym;


    private static GymManager gymManager = new GymManager();

    private GymManager() {
        gymArrayList = new ArrayList<Gym>();
        smallGym = new Gym("Small Gym");
        bigGym = new Gym("Big Gym");
        workoutGym = new Gym("Workout Gym");
        tennisGym = new Gym("Tennis Gym");


        gymArrayList.add(smallGym);
        gymArrayList.add(bigGym);
        gymArrayList.add(workoutGym);
        gymArrayList.add(tennisGym);

    }


    public static GymManager getInstance() {
        return gymManager;
    }


    //in readXML we read the XML file that has all the reservations inside it and we add the reservations
    //to our Gyms

    public void readXML(InputStream inStr) {
        try {

            //We have to make sure the Gyms are empty
            for (int j = 0; j < 4; j++) {
                ArrayList<Reservation> GymArraylistt = getGymArrayList(j);
                GymArraylistt.clear();
            }

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource inSrc = new InputSource(inStr );
            Document doc = builder.parse(inSrc);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Reservation");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    String gymname = element.getElementsByTagName("gymname").item(0).getTextContent();
                    String from = element.getElementsByTagName("from").item(0).getTextContent();
                    String to = element.getElementsByTagName("to").item(0).getTextContent();
                    String date = element.getElementsByTagName("date").item(0).getTextContent();
                    String person = element.getElementsByTagName("person").item(0).getTextContent();
                    String reason = element.getElementsByTagName("reason").item(0).getTextContent();

                    if (gymname.equals("Small Gym") == true) {
                        smallGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Big Gym") == true) {
                        bigGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Workout Gym") == true) {
                        workoutGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Tennis Gym") == true) {
                        tennisGym.addReservation(gymname, from, to, date, person, reason);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    //in getGymReservationData we search for a certain gyms' data and return it to the caller
    public ArrayList<String> getGymReservationData(int sel, String date) {

        ArrayList<Reservation> arr = null;
        ArrayList<String> gymItemsArrayList = new ArrayList<String>();
        if (sel == 0) {
            arr = smallGym.getArrayList();
        } else if (sel == 1) {
            arr = bigGym.getArrayList();
        } else if (sel == 2) {
            arr = workoutGym.getArrayList();
        } else if (sel == 3) {
            arr = tennisGym.getArrayList();
        }
        String data;

        for (Reservation a: arr) {
            if (a.getDate().equals(date) == true) {
                data = ("Gym: " + a.getGym() + "\nReservation time: " + a.getFrom() + "-" + a.getTo()
                        + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                        "\nReason for reservation: " + a.getReason());
                gymItemsArrayList.add(data);
            }
        }
        return gymItemsArrayList;
    }

    //Here we search the data for certain persons reservations and return it to caller
    public ArrayList<String> getGymReservationDataByUsername2(String username) {


        ArrayList<String> gymItemsArrayList = new ArrayList<String>();

        String data;

        for (int j = 0; j < 4; j++) {
            ArrayList<Reservation> listOfGyms = gymArrayList.get(j).getArrayList();


            for (Reservation a : listOfGyms) {
                if (a.getPerson().equals(username) == true) {
                    data = ("Gym: " + a.getGym() + "\nReservation time: " + a.getFrom() + "-" + a.getTo()
                            + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                            "\nReason for reservation: " + a.getReason());
                    gymItemsArrayList.add(data);
                }

            }
        }
        return gymItemsArrayList;
    }


    public ArrayList<Reservation> getGymArrayList(int index) {
        ArrayList<Reservation> arr = gymArrayList.get(index).getArrayList();
        return arr;
    }

    //Here we get the wanted data from our caller and we give it forward to gym to add
    //the certain reservation to certain gyms' reservation list and if we manage to add
    //it we return true and if not we return false
    public boolean addReservationAdder(String gymi, String starttitime, String enditime,
                                       String datee, String persoon, String reasoon){

        if (gymi.equals("Small Gym") == true) {

            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                smallGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                return false;
            }
        } else if (gymi.equals("Big Gym") == true) {

            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                bigGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                return false;
            }

        } else if (gymi.equals("Workout Gym") == true) {
            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                workoutGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                return false;
            }
        } else if (gymi.equals("Tennis Gym") == true) {
            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                tennisGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    //Here we check if the reservation can be done. If the reservation can be done we return true
    //otherwise we return false
    public boolean checkOtherReservations(String gymname, String startTime, String endTime, String date) {

        ArrayList<Reservation> arr = null;

        if (gymname.equals("Small Gym") == true) {
            arr = smallGym.getArrayList();
        } else if (gymname.equals("Big Gym") == true) {
            arr = bigGym.getArrayList();
        } else if (gymname.equals("Workout Gym") == true) {
            arr = workoutGym.getArrayList();
        } else if (gymname.equals("Tennis Gym") == true) {
            arr = tennisGym.getArrayList();
        }

        //If there is no reservations in the gym at all, we can make reservation anyways
        if (arr.isEmpty() == true) {
            return true;
        }

        Date compareStartingTime = null, compareEndingTime= null, startingTime = null, endingTime = null;
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("hh:mm");

        for (Reservation a: arr) {
            if (a.getDate().equals(date) == true) {
                //we see that the dates are the same so lets check if the reservation times across eachother

                try {
                    compareStartingTime = simpleDateFormat.parse(a.getFrom());
                    compareEndingTime = simpleDateFormat.parse(a.getTo());
                    startingTime = simpleDateFormat.parse(startTime);
                    endingTime = simpleDateFormat.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (((startingTime.getTime() > compareEndingTime.getTime()) ||
                        (endingTime.getTime() < compareStartingTime.getTime())) != true) {
                    return false;
                }
            } else {
            }
        }
        return true;
    }


    //here we simply delete the certain reservation
    public void deleteReservation2(int sel2, String user) {
        int index = -1;
        int wantedGymIndex = 0;
        int wantedIndex = 0;
        int smallGymIndex = -1;
        int bigGymIndex = -1;
        int workoutGymIndex = -1;
        int tennisGymIndex = -1;

        for (int j = 0; j < 4; j++) {
            ArrayList<Reservation> arrays = getGymArrayList(j);

            for (Reservation a: arrays) {

                if (a.getGym().equals("Small Gym"))
                    smallGymIndex++;
                if (a.getGym().equals("Big Gym"))
                    bigGymIndex++;
                if (a.getGym().equals("Workout Gym"))
                    workoutGymIndex++;
                if (a.getGym().equals("Tennis Gym"))
                    tennisGymIndex++;

                if (a.getPerson().equals(user) == true) {
                    index ++;
                    if (index == sel2) {
                        if (a.getGym().equals("Small Gym"))
                            wantedIndex = smallGymIndex;
                        if (a.getGym().equals("Big Gym"))
                            wantedIndex = bigGymIndex;
                        if (a.getGym().equals("Workout Gym"))
                            wantedIndex = workoutGymIndex;
                        if (a.getGym().equals("Tennis Gym"))
                            wantedIndex = tennisGymIndex;

                        wantedGymIndex = j;
                    }
                }
            }
        }
        gymArrayList.get(wantedGymIndex).deleteReservation(wantedIndex);
    }
}
