package com.example.n6723.vk11uusyritys;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NewReservation extends AppCompatActivity /*implements NavigationView.OnNavigationItemSelectedListener */{

    Context context = null;
    private ArrayAdapter mAdapter, mAdapter2;
    GymManager gymManager = GymManager.getInstance();
    XMLWriter xmlWriter = new XMLWriter();
    CSVWriter csvWriter = new CSVWriter();
    MainActivity mainActivity = new MainActivity();
    String username;
    InputStream inStr;
    Spinner spinner;
    ArrayList<String> gymData;
    ListView listView;

    // ########## USE, PURPOSE, TIME #######
    EditText kaytto;
    public String purpose = "";
    private String timeStart = "00:00";
    private String timeEnd = "23:59";
    // ################################################

    // ############# DATE ###################
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener; // HOX
    private static final String TAG = "MainActivity";
    public String date;
    // #######################################

    // ######## START TIME #################
    private TextView timelist1;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener; // HOX
    private static final String TAG2 = "MainActivity";
    private int testTime1 = 0000;
    //###################################################

    // ######## END TIME #################
    private TextView timelist2;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener2; // HOX
    private static final String TAG3 = "MainActivity";
    private int testTime2 = 2359;
    //###################################################
    int toastIndex;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_new_reservation);


        //###### Set this date as a default ##########################
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month++;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        date = day + ":" + month + ":" + year;
        String[] date1parts = date.split(":");
        String day1 = date1parts[0];
        String month1 = date1parts[1];
        String year1 = date1parts[2];

        if (day1.length() == 1) {
            day1 = "0" + day1;
        }
        if (month1.length() == 1) {
            month1 = "0" + month1;
        }
        date = day1 + "." + month1 + "." + year1;
        //######################################################

        username = mainActivity.getUsername();
        spinner = findViewById(R.id.spinner);
        kaytto = (EditText) findViewById(R.id.tarkoitus);


        // ############ date selection ############################
        mDisplayDate = (TextView) findViewById(R.id.tvDate);
        listView = (ListView) findViewById(R.id.listView);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(
                        NewReservation.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = day + ":" + month + ":" + year; //päivämäärä tallentuu tähän

                String[] date2parts = date.split(":");
                String day2 = date2parts[0];
                String month2 = date2parts[1];
                String year2 = date2parts[2];

                if (day2.length() == 1) {
                    day2 = "0" + day2;

                }
                if (month2.length() == 1) {
                    month2 = "0" + month2;
                }

                date =  day2+"."+month2+"."+year2;
                mDisplayDate.setText(date);

            }
        };
        // ################################################################


        // ############### Start time selection ############################
        // Open to user clock view and saves the time to a variable 'timeStart'

        timelist1 = (TextView) findViewById(R.id.time1);

        timelist1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal2 = Calendar.getInstance();
                int hour = cal2.get(Calendar.HOUR);
                int minute = cal2.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(
                        NewReservation.this,
                        android.R.style.Widget_Holo_Light,
                        mTimeSetListener,
                        hour, minute, true);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });
        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Log.d(TAG2, "onTimeSet: hh/mm: " + hourOfDay + "/" + minute);

                timeStart = hourOfDay + ":" + minute; //päivämäärä tallentuu tähän

                timeStart = hourOfDay + ":" + minute;
                String[] sparts = timeStart.split(":");
                String stestTime = sparts[0];
                String slastPart = sparts[1];

                if (stestTime.length() == 1) {
                    stestTime = "0" + stestTime;
                }
                if (slastPart.length() == 1) {
                    slastPart = "0" + slastPart;
                }

                timeStart = stestTime + ":" + slastPart;
                testTime1 = Integer.parseInt(stestTime) + Integer.parseInt(slastPart);

                if (testTime1 >= testTime2) {
                    timeStart = "00:00";
                    Toast.makeText(getBaseContext(), "Ending time must be bigger than starting time!", Toast.LENGTH_LONG).show();

                }

                timelist1.setText(timeStart);

            }
        };
        // ###########################################################


        // ############### end time selection ############################
        // Open to user clock view and saves the time to a variable 'timeEnd'

        timelist2 = (TextView) findViewById(R.id.time2);

        timelist2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal3 = Calendar.getInstance();
                int hour2 = cal3.get(Calendar.HOUR);
                int minute2 = cal3.get(Calendar.MINUTE);

                TimePickerDialog timeDialog2 = new TimePickerDialog(
                        NewReservation.this,
                        android.R.style.Widget_Holo_DropDownItem_Spinner,
                        mTimeSetListener2,
                        hour2, minute2, true);
                timeDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog2.show();
            }
        });
        mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay2, int minute2) {
                Log.d(TAG3, "onTimeSet: hh:mm: " + hourOfDay2 + ":" + minute2);

                timeEnd = hourOfDay2 + ":" + minute2;
                String[] eparts = timeEnd.split(":");
                String etestTime = eparts[0];
                String elastPart = eparts[1];

                if (etestTime.length() == 1) {
                    etestTime = "0" + etestTime;

                }
                if (elastPart.length() == 1) {
                    elastPart = "0" + elastPart;
                }

                timeEnd = etestTime + ":" + elastPart;
                testTime2 = Integer.parseInt(etestTime) + Integer.parseInt(elastPart);

                if (testTime1 >= testTime2) {
                    timeEnd = "23:59";
                    Toast.makeText(getBaseContext(), "Ending time must be bigger than starting time!", Toast.LENGTH_LONG).show();
                }
                timelist2.setText(timeEnd);
            }
        };
        // ####################################################################

        ArrayList<String> gymNameArrayList = new ArrayList<String>();
        gymData = new ArrayList<String>();
        listView = findViewById(R.id.listView);
        context = NewReservation.this;

        try {
            inStr = context.openFileInput("gymXML.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        gymManager.readXML(inStr);

        gymNameArrayList.add("Small Gym");
        gymNameArrayList.add("Big Gym");
        gymNameArrayList.add("Workout Gym");
        gymNameArrayList.add("Tennis Gym");

        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,gymNameArrayList);
        spinner.setAdapter(mAdapter);
        toastIndex = 0;
    }

    // Search all reservations
    public void pushSearch(View v) {

        purpose = kaytto.getText().toString();

        int selection = spinner.getSelectedItemPosition();

        gymData = gymManager.getGymReservationData(selection, date);
        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymData);
        listView.setAdapter(mAdapter2);
        if (gymData.isEmpty() == true)
            Toast.makeText(getBaseContext(), "There are no reservations for this date!", Toast.LENGTH_LONG).show();
    }

    // Here we create new reservation
    public void pushAdd(View v){

        purpose = kaytto.getText().toString();

        int selection = spinner.getSelectedItemPosition();
        String gymname = "";

        if (selection == 0) {
            gymname = "Small Gym";
        } else if (selection == 1) {
            gymname = "Big Gym";
        } else if (selection == 2) {
            gymname = "Workout Gym";
        } else if (selection == 3) {
            gymname = "Tennis Gym";
        }

        if (purpose.equals("") == true) {
            Toast.makeText(getBaseContext(), "Please add purpose!", Toast.LENGTH_LONG).show();
        } else if ((gymManager.addReservationAdder(gymname,timeStart, timeEnd, date, username, purpose)) == false) {
            Toast.makeText(getBaseContext(), ("Reservation for " +date+ " couldn't be made!"), Toast.LENGTH_LONG).show();
        } else {
            if (toastIndex == 0)
                Toast.makeText(getBaseContext(), "Reservation made!", Toast.LENGTH_LONG).show();
            csvWriter.wCSV(context);
            xmlWriter.writeXML(context);
            pushSearch(v);
        }
    }

    // Here we create weekly reservation
    public void pushAddWeeklyReservation(View v) throws ParseException {
        purpose = kaytto.getText().toString();
        if(purpose.equals("") == true) {
            Toast.makeText(getBaseContext(), "Please add purpose!", Toast.LENGTH_LONG).show();
        } else {

            Calendar calendar = Calendar.getInstance();

            SimpleDateFormat formatForWantedDate = new SimpleDateFormat("dd.MM.yyyy");

            Date wantedDate = formatForWantedDate.parse(date);
            calendar.setTime(wantedDate);

            String wantedDateString;



            SimpleDateFormat deitFormaat = new SimpleDateFormat("dd.MM.yyyy");
            toastIndex = 1;
            for (int i = 0; i < 180; i = i + 7) {
                if (i == 0) {
                    wantedDateString = deitFormaat.format(calendar.getTime());
                } else {
                    calendar.add(Calendar.DAY_OF_YEAR, 7);
                    wantedDateString = deitFormaat.format(calendar.getTime());
                }
                date = wantedDateString;
                pushAdd(v);
            }
            toastIndex = 0;
        }
    }

}