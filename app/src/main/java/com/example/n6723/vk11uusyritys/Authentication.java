package com.example.n6723.vk11uusyritys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/*
This class tests that user is not a bot using random numbers after login.

*/
public class Authentication extends Activity {
    String username;
    String usersNumbersString;
    TextView changingText = null;
    EditText authNumbers = null;
    String randomnumbers = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        Context context = Authentication.this;
        changingText = findViewById(R.id.authentication);
        buttonsetup();
        username = getIntent().getStringExtra("USERNAME");
        editNumbers();
        randomnumbers = getRandomNumber();
        changingText.setText("Authenticator:\nEnter the following digits:"+ randomnumbers);

    }


    private void buttonsetup(){
        Button okbutton = findViewById(R.id.ok);
        okbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                authenticateUser();
            }
        });

    }

    private void authenticateUser(){
        if (randomnumbers.equals(usersNumbersString)){
            //Authentication successful, goes to mainactivity with username
            Intent myIntent = new Intent(Authentication.this, MainActivity.class);
            myIntent.putExtra("USERNAME", username);
            Authentication.this.startActivity(myIntent);
            finish();
        }
        else{
            Toast.makeText(getBaseContext(), "Authentication failed!", Toast.LENGTH_LONG).show();
        }


    }


    public static String getRandomNumber() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        //returns 6 digit random number as a string
        return String.format("%06d", number);
    }



    private void editNumbers() {
        authNumbers = findViewById(R.id.number);
        authNumbers.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                usersNumbersString = authNumbers.getText().toString();
            }
        });

    }


}
