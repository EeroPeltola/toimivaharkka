package com.example.n6723.vk11uusyritys;


import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class XMLWriter {

    GymManager gymManager = GymManager.getInstance();


    //Here we simply write our reservation data to xml file
    public void writeXML(Context context) {
        try{
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("gymXML.txt", Context.MODE_PRIVATE));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<Reservations>\n");

            for (int j = 0; j < 4; j++) {
                ArrayList<Reservation> Gym = gymManager.getGymArrayList(j);

                for (int i=0; i<Gym.size(); i++){
                    stringBuilder.append("  <Reservation>\n");
                    stringBuilder.append("      <gymname>" + Gym.get(i).getGym() + "</gymname>\n");
                    stringBuilder.append("      <from>" + Gym.get(i).getFrom() + "</from>\n");
                    stringBuilder.append("      <to>" + Gym.get(i).getTo() + "</to>\n");
                    stringBuilder.append("      <date>" + Gym.get(i).getDate() + "</date>\n");
                    stringBuilder.append("      <person>" + Gym.get(i).getPerson() + "</person>\n");
                    stringBuilder.append("      <reason>" + Gym.get(i).getReason() + "</reason>\n");
                    stringBuilder.append("  </Reservation>\n");
                }
            }
            //#######################################TÄHÄN LOPPUU###############

            stringBuilder.append("</Reservations>");

            String xmlText = stringBuilder.toString();
            ows.write(xmlText);
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Error in the input!");
        } finally {
            System.out.println("XML file has been written!");

        }
    }






}
