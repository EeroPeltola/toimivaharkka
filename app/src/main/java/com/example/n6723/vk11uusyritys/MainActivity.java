package com.example.n6723.vk11uusyritys;


import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    public String date;
    private static String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = getIntent().getStringExtra("USERNAME");

    }

    public void startNewReservation(View v){
        Intent intent = new Intent(this, NewReservation.class);
        startActivity(intent);
    }
    public void startYourReservations(View v){
        Intent intent = new Intent(this, YourReservations.class);
        startActivity(intent);
    }
    public void startSettings(View v){
        Intent intent = new Intent(this, SettingsMenu.class);
        startActivity(intent);
    }
    public void startLogout(View v){
        System.exit(0);
    }

    public static String getUsername(){
        return username;
    }

}
