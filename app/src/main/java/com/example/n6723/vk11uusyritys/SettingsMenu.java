package com.example.n6723.vk11uusyritys;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class SettingsMenu extends AppCompatActivity {

    String text;
    String filename = "userList.txt";
    Context context = null;
    EditText textInput;
    EditText textInput2;
    static String username = null;
    static String address = null;
    static String phonenumber = null;
    static String rights = null;
    static String salt = null;
    static String saltedpassword = null;
    static String usernameInFile = null;
    String newAddress = null;
    String newPhoneNumber = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = SettingsMenu.this;
        editAddress();
        editPhoneNumber();
        username = MainActivity.getUsername();
        readFile(null);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }



    public void readFile(View v) {//Reads the userlist to show Address and Phonenumber

        try {

            InputStream ins = context.openFileInput(filename);
            String message = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";

            while ((s = br.readLine()) != null) {
                String[] pieces = s.split(":");

                if (pieces[0].equals(username)) {
                    address = pieces[4];
                    phonenumber = pieces[5];
                    rights = pieces[1];
                }

            }

            final TextView changingText = findViewById(R.id.profileText);

            message = ("Username: " + username + "\nAddress: " + address + "\nPhone number: " + phonenumber + "\nRights: " + rights);


            changingText.setText(message);
            ins.close();
            br.close();
        } catch (IOException e) {
            Log.e("IOExceoption", "Virhe syöttessä");
        }

    }


    public void writeFile(View v) {//Edits the userlist with new entered phonenumber/address

        String profileList = "";

        try {
            Scanner scanner = new Scanner(new InputStreamReader(context.openFileInput("userList.txt")));
            scanner.useDelimiter("[:\n]");

            while (scanner.hasNext()) {

                usernameInFile = scanner.next();
                rights = scanner.next();
                salt = scanner.next();
                saltedpassword = scanner.next();
                address = scanner.next();
                phonenumber = scanner.next();

                if (usernameInFile.equals(username)) {
                        if (newAddress == null|| newAddress.isEmpty()){
                            newAddress = address;
                        }
                        if (newPhoneNumber == null || newPhoneNumber.isEmpty()){
                            newPhoneNumber = phonenumber;
                        }
                        profileList = profileList + (username + ":" + rights + ":" + salt + ":" + saltedpassword + ":" + newAddress + ":" + newPhoneNumber + "\n");

                } else {
                    profileList = profileList + (usernameInFile + ":" + rights + ":" + salt + ":" + saltedpassword + ":" + address + ":" + phonenumber + "\n");
                }

            }
            scanner.close();

            try {
                OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
                ows.write(profileList);
                ows.close();

            } catch (IOException e) {
                Log.e("IOExceoption", "Virhe");
            }

        } catch (Exception e) {
        }

        readFile(null);

    }


    private void editAddress() {
        textInput = findViewById(R.id.newAddress);
        textInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                newAddress = textInput.getText().toString();
            }
        });

    }

        private void editPhoneNumber(){
            textInput2 = findViewById(R.id.newPhonenumber);
            textInput2.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    newPhoneNumber = textInput2.getText().toString();
                }
            });

    }
}